package net.shopxx.boot;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class BeanFactory implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @SuppressWarnings("static-access")
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * Get Bean
     *
     * @param beanName BEAN 名称
     * @return BEAN
     */
    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    /**
     * Get Bean
     *
     * @param clazz 类名
     * @return bean
     */
    public static Object getBean(Class<?> clazz) {
        return applicationContext.getBean(clazz);
    }

}

package net.shopxx.boot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceResolverRequestFilter;
import org.springframework.mobile.device.site.SitePreferenceRequestFilter;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.RequestContextFilter;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import net.shopxx.captcha.CaptchaFilter;
import net.shopxx.filter.PageCachingFilter;
import net.shopxx.security.LogoutFilter;

/**
 */
@Configuration
public class FilterConfig {
	@Bean
	public FilterRegistrationBean requestContxtFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		RequestContextFilter requestContxtFilter = new RequestContextFilter();
		registrationBean.setFilter(requestContxtFilter);
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean characterEncodingFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("utf-8");
		characterEncodingFilter.setForceRequestEncoding(true);
		characterEncodingFilter.setForceResponseEncoding(true);
		characterEncodingFilter.setForceEncoding(true);
		registrationBean.setFilter(characterEncodingFilter);
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/*");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}
	
    @Bean
    public FilterRegistrationBean deviceResolverRequestFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        DeviceResolverRequestFilter deviceResolverRequestFilter = new DeviceResolverRequestFilter();
        registrationBean.setFilter(deviceResolverRequestFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean sitePreferenceRequestFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        SitePreferenceRequestFilter sitePreferenceRequestFilter = new SitePreferenceRequestFilter();
        registrationBean.setFilter(sitePreferenceRequestFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean etagHeaderFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        ShallowEtagHeaderFilter etagHeaderFilter = new ShallowEtagHeaderFilter();
        registrationBean.setFilter(etagHeaderFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/*");
        registrationBean.setUrlPatterns(urlPatterns);
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean memberLoginCaptchaFilter(@Autowired CaptchaFilter captchaFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(captchaFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/member/login");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("captchaType", "MEMBER_LOGIN");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean businessLoginCaptchaFilter(@Autowired CaptchaFilter captchaFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(captchaFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/business/login");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("captchaType", "BUSINESS_LOGIN");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean adminLoginCaptchaFilter(@Autowired CaptchaFilter captchaFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(captchaFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/admin/login");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("captchaType", "ADMIN_LOGIN");
        return registrationBean;
    }
    
	@Bean
	public FilterRegistrationBean openEntityManagerInViewFilter() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		OpenEntityManagerInViewFilter openEntityManagerInViewFilter = new OpenEntityManagerInViewFilter();
		registrationBean.setFilter(openEntityManagerInViewFilter);
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/member/login");
		urlPatterns.add("/member/logout");
		urlPatterns.add("/business/login");
		urlPatterns.add("/business/logout");
		urlPatterns.add("/admin/login");
		urlPatterns.add("/admin/logout");
		registrationBean.setUrlPatterns(urlPatterns);
		return registrationBean;
	}
	
	
	@Bean
	public net.shopxx.security.LogoutFilter logoutFilter(){
	    return new LogoutFilter();
	}
	
    @Bean
    public FilterRegistrationBean registration(net.shopxx.security.LogoutFilter logoutFilter) {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean(logoutFilter);
        registrationBean.setEnabled(false);
        return registrationBean;
    }
	
    
    //缓存区域
    @Bean
    public FilterRegistrationBean indexPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter indexPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(indexPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "indexPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean storeIndexPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter storeIndexPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(storeIndexPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/store/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "storeIndexPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean articleListPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter articleListPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(articleListPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/article/list/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "articleListPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean articleDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter articleDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(articleDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/article/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "articleDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean productListPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter productListPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(productListPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/product/list/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "productListPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean productDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter productDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(productDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/product/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "productDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean brandListPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter brandListPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(brandListPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/brand/list/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "brandListPage");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean brandDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter brandDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(brandDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/brand/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "brandDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean productCategoryPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter productCategoryPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(productCategoryPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/product_category");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "productCategoryPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean promotionDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter promotionDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(promotionDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/promotion/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "promotionDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean friendLinkPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter friendLinkPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(friendLinkPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/friend_link");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "friendLinkPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean consultationDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter consultationDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(consultationDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/consultation/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "consultationDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean reviewDetailPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter reviewDetailPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(reviewDetailPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/review/detail/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "reviewDetailPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean areaPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter areaPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(areaPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/common/area");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "areaPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean baseJsPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter baseJsPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(baseJsPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/resources/common/js/base.js");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "baseJsPage");
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean sitemapPageCachingFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        PageCachingFilter sitemapPageCachingFilter = new PageCachingFilter();
        registrationBean.setFilter(sitemapPageCachingFilter);
        List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/sitemap/*");
        registrationBean.setUrlPatterns(urlPatterns);
        registrationBean.addInitParameter("cacheName", "sitemapPage");
        return registrationBean;
    }
}

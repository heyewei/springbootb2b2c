package net.shopxx.boot;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import net.shopxx.security.AuthorizingRealm;

@Configuration
public class ShiroConfig {
    
    @Autowired
    private Environment env;
    
    @Autowired
    private net.shopxx.security.LogoutFilter logoutFilter;
    
    @Bean
    public ShiroFilterFactoryBean shiroFilter(org.apache.shiro.mgt.SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        shiroFilterFactoryBean.setUnauthorizedUrl(env.getProperty("security.unauthorized_url"));
        
        Map<String,String> filterChainDefinitionMap = new LinkedHashMap<>();
        
        filterChainDefinitionMap.put("/member/register","anon");
        filterChainDefinitionMap.put("/member/register/**","anon");
        filterChainDefinitionMap.put("/member/login","memberAuthc");
        filterChainDefinitionMap.put("/member/logout","logout");

        filterChainDefinitionMap.put("/order/**","memberAuthc,perms[member]");

        filterChainDefinitionMap.put("/consultation/add/*","memberAuthc,perms[member]");
        filterChainDefinitionMap.put("/consultation/save","memberAuthc,perms[member]");

        filterChainDefinitionMap.put("/review/add/*","memberAuthc,perms[member]");
        filterChainDefinitionMap.put("/review/save","memberAuthc,perms[member]");

        filterChainDefinitionMap.put("/member/**","memberAuthc,perms[member]");

        filterChainDefinitionMap.put("/business/register/**","anon");
        filterChainDefinitionMap.put("/business/login","businessAuthc");
        filterChainDefinitionMap.put("/business/logout","logout");

        filterChainDefinitionMap.put("/business/index","businessAuthc,perms[business:index]");

        filterChainDefinitionMap.put("/business/index/**","businessAuthc,perms[business:index]");
        filterChainDefinitionMap.put("/business/product/**","businessAuthc,perms[business:product]");
        filterChainDefinitionMap.put("/business/stock/**","businessAuthc,perms[business:stock]");
        filterChainDefinitionMap.put("/business/product_notify/**","businessAuthc,perms[business:productNotify]");
        filterChainDefinitionMap.put("/business/consultation/**","businessAuthc,perms[business:consultation]");
        filterChainDefinitionMap.put("/business/review/**","businessAuthc,perms[business:review]");
        filterChainDefinitionMap.put("/business/order/**","businessAuthc,perms[business:order]");
        filterChainDefinitionMap.put("/business/print/**","businessAuthc,perms[business:print]");
        filterChainDefinitionMap.put("/business/delivery_template/**","businessAuthc,perms[business:deliveryTemplate]");
        filterChainDefinitionMap.put("/business/delivery_center/**","businessAuthc,perms[business:deliveryCenter]");
        filterChainDefinitionMap.put("/business/aftersales/**","businessAuthc,perms[business:aftersales]");
        filterChainDefinitionMap.put("/business/store/register","businessAuthc,perms[business:storeRegister]");
        filterChainDefinitionMap.put("/business/store/setting","businessAuthc,perms[business:storeSetting]");
        filterChainDefinitionMap.put("/business/store/payment","businessAuthc,perms[business:storePayment]");
        filterChainDefinitionMap.put("/business/store/reapply","businessAuthc,perms[business:storeReapply]");
        filterChainDefinitionMap.put("/business/store_product_category/**","businessAuthc,perms[business:storeProductCategory]");
        filterChainDefinitionMap.put("/business/store_product_tag/**","businessAuthc,perms[business:storeProductTag]");
        filterChainDefinitionMap.put("/business/category_application/**","businessAuthc,perms[business:categoryApplication]");
        filterChainDefinitionMap.put("/business/shipping_method/**","businessAuthc,perms[business:shippingMethod]");
        filterChainDefinitionMap.put("/business/area_freight_config/**","businessAuthc,perms[business:areaFreightConfig]");
        filterChainDefinitionMap.put("/business/store_ad_image/**","businessAuthc,perms[business:storeAdImage]");
        filterChainDefinitionMap.put("/business/aftersales_setting/**","businessAuthc,perms[business:aftersalesSetting]");
        filterChainDefinitionMap.put("/business/promotion_plugin/**","businessAuthc,perms[business:promotionPlugin]");
        filterChainDefinitionMap.put("/business/**promotion/**","businessAuthc,perms[business:promotion]");
        filterChainDefinitionMap.put("/business/coupon/**","businessAuthc,perms[business:coupon]");
        filterChainDefinitionMap.put("/business/business_deposit/**","businessAuthc,perms[business:businessDeposit]");
        filterChainDefinitionMap.put("/business/business_cash/**","businessAuthc,perms[business:businessCash]");
        filterChainDefinitionMap.put("/business/profile/**","businessAuthc,perms[business:profile]");
        filterChainDefinitionMap.put("/business/password/**","businessAuthc,perms[business:password]");
        filterChainDefinitionMap.put("/business/message/**","businessAuthc,perms[business:message]");
        filterChainDefinitionMap.put("/business/message_group/**","businessAuthc,perms[business:messageGroup]");
        filterChainDefinitionMap.put("/business/instant_message/**","businessAuthc,perms[business:instantMessage]");
        filterChainDefinitionMap.put("/business/order_statistic/**","businessAuthc,perms[business:orderStatistic]");
        filterChainDefinitionMap.put("/business/fund_statistic/**","businessAuthc,perms[business:fundStatistic]");
        filterChainDefinitionMap.put("/business/product_ranking/**","businessAuthc,perms[business:productRanking]");

        filterChainDefinitionMap.put("/business/**","businessAuthc");

        filterChainDefinitionMap.put("/admin","anon");
        filterChainDefinitionMap.put("/admin/","anon");
        filterChainDefinitionMap.put("/admin/login","adminAuthc");
        filterChainDefinitionMap.put("/admin/logout","logout");

        filterChainDefinitionMap.put("/admin/business/**","adminAuthc,perms[admin:business]");
        filterChainDefinitionMap.put("/admin/store/**","adminAuthc,perms[admin:store]");
        filterChainDefinitionMap.put("/admin/store_category/**","adminAuthc,perms[admin:storeCategory]");
        filterChainDefinitionMap.put("/admin/store_rank/**","adminAuthc,perms[admin:storeRank]");
        filterChainDefinitionMap.put("/admin/business_attribute/**","adminAuthc,perms[admin:businessAttribute]");
        filterChainDefinitionMap.put("/admin/business_cash/**","adminAuthc,perms[admin:businessCash]");
        filterChainDefinitionMap.put("/admin/category_application/**","adminAuthc,perms[admin:categoryApplication]");
        filterChainDefinitionMap.put("/admin/business_deposit/**","adminAuthc,perms[admin:businessDeposit]");

        filterChainDefinitionMap.put("/admin/product/**","adminAuthc,perms[admin:product]");
        filterChainDefinitionMap.put("/admin/stock/**","adminAuthc,perms[admin:stock]");
        filterChainDefinitionMap.put("/admin/product_category/**","adminAuthc,perms[admin:productCategory]");
        filterChainDefinitionMap.put("/admin/product_tag/**","adminAuthc,perms[admin:productTag]");
        filterChainDefinitionMap.put("/admin/parameter/**","adminAuthc,perms[admin:parameter]");
        filterChainDefinitionMap.put("/admin/attribute/**","adminAuthc,perms[admin:attribute]");
        filterChainDefinitionMap.put("/admin/specification/**","adminAuthc,perms[admin:specification]");
        filterChainDefinitionMap.put("/admin/brand/**","adminAuthc,perms[admin:brand]");

        filterChainDefinitionMap.put("/admin/order/**","adminAuthc,perms[admin:order]");
        filterChainDefinitionMap.put("/admin/print/**","adminAuthc,perms[admin:print]");
        filterChainDefinitionMap.put("/admin/order_payment/**","adminAuthc,perms[admin:orderPayment]");
        filterChainDefinitionMap.put("/admin/order_refunds/**","adminAuthc,perms[admin:orderRefunds]");
        filterChainDefinitionMap.put("/admin/order_shipping/**","adminAuthc,perms[admin:orderShipping]");
        filterChainDefinitionMap.put("/admin/order_returns/**","adminAuthc,perms[admin:orderReturns]");
        filterChainDefinitionMap.put("/admin/delivery_center/**","adminAuthc,perms[admin:deliveryCenter]");
        filterChainDefinitionMap.put("/admin/delivery_template/**","adminAuthc,perms[admin:deliveryTemplate]");
        filterChainDefinitionMap.put("/admin/aftersales/**","adminAuthc,perms[admin:aftersales]");

        filterChainDefinitionMap.put("/admin/member/**","adminAuthc,perms[admin:member]");
        filterChainDefinitionMap.put("/admin/member_rank/**","adminAuthc,perms[admin:memberRank]");
        filterChainDefinitionMap.put("/admin/member_attribute/**","adminAuthc,perms[admin:memberAttribute]");
        filterChainDefinitionMap.put("/admin/point/**","adminAuthc,perms[admin:point]");
        filterChainDefinitionMap.put("/admin/member_deposit/**","adminAuthc,perms[admin:memberDeposit]");
        filterChainDefinitionMap.put("/admin/review/**","adminAuthc,perms[admin:review]");
        filterChainDefinitionMap.put("/admin/consultation/**","adminAuthc,perms[admin:consultation]");
        filterChainDefinitionMap.put("/admin/message_config/**","adminAuthc,perms[admin:messageConfig]");

        filterChainDefinitionMap.put("/admin/distributor/**","adminAuthc,perms[admin:distributor]");
        filterChainDefinitionMap.put("/admin/distribution_cash/**","adminAuthc,perms[admin:distributionCash]");
        filterChainDefinitionMap.put("/admin/distribution_commission/**","adminAuthc,perms[admin:distributionCommission]");

        filterChainDefinitionMap.put("/admin/navigation_group/**","adminAuthc,perms[admin:navigationGroup]");
        filterChainDefinitionMap.put("/admin/navigation/**","adminAuthc,perms[admin:navigation]");
        filterChainDefinitionMap.put("/admin/article/**","adminAuthc,perms[admin:article]");
        filterChainDefinitionMap.put("/admin/article_category/**","adminAuthc,perms[admin:articleCategory]");
        filterChainDefinitionMap.put("/admin/article_tag/**","adminAuthc,perms[admin:articleTag]");
        filterChainDefinitionMap.put("/admin/friend_link/**","adminAuthc,perms[admin:friendLink]");
        filterChainDefinitionMap.put("/admin/ad_position/**","adminAuthc,perms[admin:adPosition]");
        filterChainDefinitionMap.put("/admin/ad/**","adminAuthc,perms[admin:ad]");
        filterChainDefinitionMap.put("/admin/template/**","adminAuthc,perms[admin:template]");
        filterChainDefinitionMap.put("/admin/cache/**","adminAuthc,perms[admin:cache]");

        filterChainDefinitionMap.put("/admin/promotion/**","adminAuthc,perms[admin:promotion]");
        filterChainDefinitionMap.put("/admin/coupon/**","adminAuthc,perms[admin:coupon]");
        filterChainDefinitionMap.put("/admin/seo/**","adminAuthc,perms[admin:seo]");

        filterChainDefinitionMap.put("/admin/setting/**","adminAuthc,perms[admin:setting]");
        filterChainDefinitionMap.put("/admin/area/**","adminAuthc,perms[admin:area]");
        filterChainDefinitionMap.put("/admin/payment_method/**","adminAuthc,perms[admin:paymentMethod]");
        filterChainDefinitionMap.put("/admin/shipping_method/**","adminAuthc,perms[admin:shippingMethod]");
        filterChainDefinitionMap.put("/admin/delivery_corp/**","adminAuthc,perms[admin:deliveryCorp]");
        filterChainDefinitionMap.put("/admin/payment_plugin/**","adminAuthc,perms[admin:paymentPlugin]");
        filterChainDefinitionMap.put("/admin/storage_plugin/**","adminAuthc,perms[admin:storagePlugin]");
        filterChainDefinitionMap.put("/admin/login_plugin/**","adminAuthc,perms[admin:loginPlugin]");
        filterChainDefinitionMap.put("/admin/promotion_plugin/**","adminAuthc,perms[admin:promotionPlugin]");
        filterChainDefinitionMap.put("/admin/admin/**","adminAuthc,perms[admin:admin]");
        filterChainDefinitionMap.put("/admin/role/**","adminAuthc,perms[admin:role]");
        filterChainDefinitionMap.put("/admin/audit_log/**","adminAuthc,perms[admin:auditLog]");
        
        filterChainDefinitionMap.put("/admin/order_statistic/**","adminAuthc,perms[admin:orderStatistic]");
        filterChainDefinitionMap.put("/admin/fund_statistic/**","adminAuthc,perms[admin:fundStatistic]");
        filterChainDefinitionMap.put("/admin/register_statistic/**","adminAuthc,perms[admin:registerStatistic]");
        filterChainDefinitionMap.put("/admin/product_ranking/**","adminAuthc,perms[admin:productRanking]");

        filterChainDefinitionMap.put("/admin/**","adminAuthc");
        
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        
        Map<String,Filter> filters = new LinkedHashMap<>();
        net.shopxx.security.AuthenticationFilter memberAuthenticationFilter = memberAuthenticationFilter();
        memberAuthenticationFilter.setUserClass(net.shopxx.entity.Member.class);
        memberAuthenticationFilter.setLoginUrl(env.getProperty("security.member_login_url"));
        memberAuthenticationFilter.setSuccessUrl(env.getProperty("security.member_login_success_url"));
        filters.put("memberAuthc", memberAuthenticationFilter);
        
        net.shopxx.security.AuthenticationFilter businessAuthenticationFilter = businessAuthenticationFilter();
        businessAuthenticationFilter.setUserClass(net.shopxx.entity.Business.class);
        businessAuthenticationFilter.setLoginUrl(env.getProperty("security.business_login_url"));
        businessAuthenticationFilter.setSuccessUrl(env.getProperty("security.business_login_success_url"));
        filters.put("businessAuthc", businessAuthenticationFilter);
        
        net.shopxx.security.AuthenticationFilter adminAuthenticationFilter = adminAuthenticationFilter();
        adminAuthenticationFilter.setUserClass(net.shopxx.entity.Admin.class);
        adminAuthenticationFilter.setLoginUrl(env.getProperty("security.admin_login_url"));
        adminAuthenticationFilter.setSuccessUrl(env.getProperty("security.admin_login_success_url"));
        filters.put("adminAuthc", adminAuthenticationFilter);
        
        filters.put("logout", logoutFilter);
        
        shiroFilterFactoryBean.setFilters(filters);
        return shiroFilterFactoryBean;
    }
    
    @Bean
    public net.shopxx.security.AuthenticationFilter memberAuthenticationFilter(){
        return new net.shopxx.security.AuthenticationFilter();
    }
    @Bean
    public net.shopxx.security.AuthenticationFilter businessAuthenticationFilter(){
        return new net.shopxx.security.AuthenticationFilter();
    }
    @Bean
    public net.shopxx.security.AuthenticationFilter adminAuthenticationFilter(){
        return new net.shopxx.security.AuthenticationFilter();
    }
    
    
    @Bean
    public org.apache.shiro.mgt.SecurityManager securityManager(EhCacheManager shiroCacheManager){
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        defaultWebSecurityManager.setRealm(authorizingRealm());
        defaultWebSecurityManager.setCacheManager(shiroCacheManager);
        return defaultWebSecurityManager;
    }
    
    @Bean
    public AuthorizingRealm authorizingRealm(){
        AuthorizingRealm authorizingRealm = new AuthorizingRealm();
        authorizingRealm.setAuthorizationCacheName("authorization");
        return authorizingRealm;
    }
    
    @Bean
    public EhCacheManager shiroCacheManager(@Autowired EhCacheManagerFactoryBean ehCacheManagerFactoryBean){
        EhCacheManager shiroEhCacheManager = new EhCacheManager();
        shiroEhCacheManager.setCacheManager(ehCacheManagerFactoryBean.getObject());
        return shiroEhCacheManager;
    }
    
    @Bean
    public MethodInvokingFactoryBean methodInvokingFactoryBean(org.apache.shiro.mgt.SecurityManager securityManager){
        MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
        methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
        methodInvokingFactoryBean.setArguments(securityManager);
        return methodInvokingFactoryBean;
    }
    

}
